def handler(event, _context):
    try:
        name = event["queryStringParameters"]["name"]
    except Exception:
        return {"body": "You didn't provide a name!"}
    else:
        print(f"{name} has deployed their application!")
        return {"body": f"Hello {name}, welcome to 8th Light!"}
